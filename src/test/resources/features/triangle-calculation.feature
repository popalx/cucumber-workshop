Feature: Determine the type of triangle
  Triangles can be defined as different types. A triangle with equal sides is a equilateral triangle, with two equal
  sides is called a isosceles triangle and other triangles are called scalene triangles.

  Scenario: A triangle with all equal sides
    Given Input calculator is a available
    When 3 equals sides are inputted
    Then the triangle is qualified as 'equilateral'
    
  Scenario: A triangle with two equal sides
    Given Input calculator is a available
    When 2 equals sides are inputted
    Then the triangle is qualified as 'isosceles'
    
  Scenario: A triangle with no equal sides
    Given Input calculator is a available
    When 1 equals sides are inputted
    Then the triangle is qualified as 'scalene'
    
  Scenario: A triangle with all equal sides alternative
    Given Input calculator is a available
    When I enter for side 1 value 3
    And I enter for side 2 value 3
    And I enter for side 3 value 3
    Then the triangle is qualified as 'equilateral'
    
  Scenario Outline: Triangles
    Given Input calculator is a available
    When I enter for side 1 value <side value 1>
    And I enter for side 2 value <side value 2>
    And I enter for side 3 value <side value 3>
    Then the triangle is qualified as '<triangle type>'
  Examples:
  | side value 1 | side value 2 | side value 3 | triangle type |
  | 1            | 1            | 1            | equilateral   |
  | 1            | 1            | 3            | isosceles     |
  | 1            | 2            | 3            | scalene       |